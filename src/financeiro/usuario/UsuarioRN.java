package financeiro.usuario;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import financeiro.categoria.CategoriaRN;
import financeiro.util.DAOFactory;
import financeiro.util.UtilLibrary;

public class UsuarioRN {

	private UsuarioDAO usuarioDAO;
	public UsuarioRN(){
		 this.usuarioDAO = DAOFactory.criarUsuarioDAO();
	}
	
	public Usuario carregar(Integer codigo){
		return this.usuarioDAO.carregar(codigo);
	}
	
	public Usuario buscarPorLogin(String login){
		return this.usuarioDAO.buscaPorLogin(login);
	}
	
	public void salvar(Usuario usuario) throws NoSuchAlgorithmException{
		
		Integer codigo = usuario.getCodigo();
		if(codigo == null || codigo == 0){
			UtilLibrary util = new UtilLibrary();
			String senhaCrp = util.encriptarSenha(usuario.getSenha());
			usuario.setSenha(senhaCrp);
			usuario.getPermissao().add("ROLE_USUARIO");			
			this.usuarioDAO.salvar(usuario);
			
			CategoriaRN categoriaRN = new CategoriaRN();
			categoriaRN.salvaEstruturaPadrao(usuario);
		}else{
			this.usuarioDAO.atualizar(usuario);
		}
		
		
		/*		 
		Integer codigo = usuario.getCodigo();
		if (codigo == null || codigo == 0) {

			usuario.getPermissao().add("ROLE_USUARIO");

			this.usuarioDAO.salvar(usuario);
			

		} else {
			this.usuarioDAO.atualizar(usuario);
		}
		*/
	}
	
	public void excluir(Usuario usuario){
		CategoriaRN categoriaRN = new CategoriaRN();
		categoriaRN.excluir(usuario);
		this.usuarioDAO.excluir(usuario);
	}
	
	public List<Usuario> listar(){
		return this.usuarioDAO.listar();
	}
}

package financeiro.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;

public class UtilLibrary {

	public Calendar testeInserirData(Integer dia){
		Calendar dataAtual = Calendar.getInstance();
		dataAtual.set(Calendar.DAY_OF_MONTH, dia);
		return dataAtual;
	}

	public Calendar obterDataDebito(Integer dia){
		
		Calendar novoDiaDataAtual = Calendar.getInstance();		
		Calendar dataDebito = Calendar.getInstance();
		dataDebito.set(Calendar.DAY_OF_MONTH, dia);
		
		if(novoDiaDataAtual.equals(dataDebito) || novoDiaDataAtual.after(dataDebito)){
			dataDebito.add(Calendar.MONTH, 1);
			return dataDebito;			
		}else{
			novoDiaDataAtual.set(Calendar.DAY_OF_MONTH, dia);			
			return novoDiaDataAtual;			
		}		
	}
	
	public Calendar complementoData(){
		Calendar data = Calendar.getInstance();
		data.add(Calendar.MONTH, 1);
		return data;		
	}
	
	public boolean debitado(Calendar data){
		Calendar dataAtual = Calendar.getInstance();
				
		Integer diaAtual = dataAtual.get(Calendar.DAY_OF_YEAR);
		Integer diaDebto = data.get(Calendar.DAY_OF_YEAR);
		
		if(diaAtual <= diaDebto){
			return false;			
		}else{
			return true;
		}		
	}
	
	public String encriptarSenha(String senha) throws NoSuchAlgorithmException{
		
		try {
			MessageDigest md = MessageDigest.getInstance("SHA");  
	        md.update(senha.getBytes());  
	        BigInteger hash = new BigInteger(1, md.digest());  
	        String senhaCriptografada = hash.toString(16);
	        return senhaCriptografada;
		} catch (Exception e) {			
			return null;
		}		
	}	
	
}
